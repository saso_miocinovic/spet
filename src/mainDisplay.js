// Default pomodoro timer values (25 minutes9

const electron = require('electron');
const {ipcRenderer: ipc,app} = electron;


const fs = require('fs');

const Config = require('electron-config');
const config = new Config();
//Get database path from main.js


const Datastore = require('nedb');
var pomodoros;
/*Timer variables*/
var pHours = 0;
var pMinutes = 0;
var pSeconds = 0;

var sbHours = 0;
var sbMinutes = 0;
var sbSeconds = 0;

var lbHours = 0;
var lbMinutes = 0;
var lbSeconds = 0;
/*//Timer variables*/
/*Variables for PAUSE / RESUME functionality*/
var startTime;
/*//Variables for PAUSE / RESUME functionality*/

var timeElapsed;
// Did user end counter manually?
var hasEndedByInput = false;

// Audio file:
var audio = new Audio('../Sounds/Ring.mp3');

function initializeTimerValues() {
    pHours = config.get('PomodoroTimer.hours');
    pMinutes = config.get('PomodoroTimer.minutes');
    pSeconds = config.get('PomodoroTimer.seconds');

    sbHours = config.get('BreakTimerShort.hours');
    sbMinutes = config.get('BreakTimerShort.minutes');
    sbSeconds = config.get('BreakTimerShort.seconds');

    lbHours = config.get('BreakTimerLong.hours');
    lbMinutes = config.get('BreakTimerLong.minutes');
    lbSeconds = config.get('BreakTimerLong.seconds');
}
// Add appropriate icons to sound button depending whether sound is enabled or disabled
function setSoundOption() {
    if(config.get('Sound.enabeled')){
        $("#sound").append("<i class='fa fa-volume-up fa-2x' aria-hidden='true'></i>");
    }else{
        $("#sound").append("<i class='fa fa-volume-off fa-2x' aria-hidden='true'></i>");
    }
}
$(document).ready(()=> {
    // Check if debug mode is enabled and thusly set availability of debug button:
    if(config.get('Debug.checked'))
        $('#mockADb').show();
    else{
        $('#mockADb').hide();
    }
    setSoundOption();

    initializeTimerValues();
    console.log(fs.existsSync(config.get('DbFilePath')));
    // Does the DB already exist?
    console.log(config.get('DbFilePath'));

    if (!fs.existsSync(config.get('DbFilePath'))){
        // If it doesn't, create it:
        console.log("Inside if");
        pomodoros = new Datastore({filename: config.get('DbFilePath'), autoload: true, corruptAlertThreshold: 1});
    }
    // else{
    //     pomodoros = Datastore({filename: config.get('DbFilePath'), autoload: true, corruptAlertThreshold: 1});
    // }
    document.getElementsByTagName("html")[0].style.visibility = "visible";


});
// Display message in the bottom bar about which action is taken by clicking on a hovered element.
$(".myButton").hover(()=>{
    if(!($(this).hasClass("disabled"))){
        //If button isn't disabeled write appropriate message to footer cmdline
        var message;
        switch(event.target.id){
            case "startCountdown":
                message = "Start the pomodoro counter.";
                break;
            case "skipNextCountdown":
                message = "Change pomodoro/break as next option.";
                break;
            case "settingsBtn":
                message = "Open settings window."
                break;
            case "showDb":
                message = "See the results of previous pomodoros.";
                break;
            case "startBreak":
                message = "Start the break.";
                break;
            case "endCountdown":
                message = "Stop / pause the countdown.";
                break;
            case "sound":
                if(config.get("Sound.enabeled")){
                    message = "Disable ring sound.";
                }else{
                    message = "Enable ring sound.";
                }
                break;
        }
        displayMessageInBottomBar(message);
    }

});
// Clear message when mouse leaves the button
$(".myButton").mouseleave(()=>{
    $('#footerText').typed({
        strings : [""],
        typeSpeed: 0
    });
});
// Set buttons availability:
switchEnableDisableButton(['#endCountdown','#startBreak']);

// Initialize pomodoro timer variables from config.json,
// it is done in function because settings may be changes after the main page was initialized
function pomodoroTime(h, m, s) {
    startTime = getCurrentDateTime();
    // Target time is current time incremented by time passed as arguments to a function
    var targetTime = (startTime.add(h, 'hours').add(m, 'minutes').add(s, 'seconds').format('YYYY/MM/DD HH:mm:ss'));
    return targetTime.toString();
}

// Method to return full current date and time in ISO_8601 format
function getCurrentDateTime() {
    var jsDate = new Date();
    // Month is zero based so we have to add 1 to it.
    var nowDate = jsDate.getFullYear() + "-" + (jsDate.getMonth() + 1) + "-" + jsDate.getDate();
    var nowHms = jsDate.getHours() + ":" + jsDate.getMinutes() + ":" + jsDate.getSeconds();
    return moment((nowDate.toString() + " " + nowHms.toString()), 'YYYY/MM/DD hh:mm:ss');
}
// Function to write data to Pomodoros array in config.json


function writePomodoroToConfig(toSaveObject) {
    console.log(toSaveObject);
    pomodoros.insert(toSaveObject, (err,doc)=>{
        console.log('Inserted', doc.duration,'With ID', doc._id);
    });
}
/*CONTROL VARIABLES FOR MAINDISPLAY:*/
// How many pomodoros ran in a session
var pomodoroCounter = 0;
// Enum for timer currently timerToRun
var timerToRun = "POMODORO";
var timerRunning = "";
// When was timer started
var startTime;
var durationString = pHours + ':' + pMinutes + ':' + pSeconds;

// Disable / enable sound effects:
$("#sound").click(()=>{
    if(config.get('Sound.enabeled')){
        // Disable sound
        config.set('Sound.enabeled',false);
        // Show disabeled sound icon <button id="sound" class="myButton"></button>
        $("#sound").html("<i class='fa fa-volume-off fa-2x' aria-hidden='true'></i>");
        //If sound is playing stop:
        audio.pause();
    }else{
        // Enable sound
        config.set('Sound.enabeled',true);
        //Show enabeled sound icon
        $("#sound").html("<i class='fa fa-volume-up fa-2x' aria-hidden='true'></i>");
    }
});
// Function to mock a few DB records (for debugging results page):
$("#mockADb").click(()=>{

    /*This method creates mock records from an arbitrary date and then increments that date's parameters
    * somewhat randomly. FOR DEBUGGING PURPOSES ONLY!!!*/
    var startTime = "2015-1-1 13:00:00";


    // Increment month value 3 times
    for (var month = 3; month >=0; month--) {
        // Add one or two months at random
        startTime = moment(startTime).add(Math.floor((Math.random() * 2) + 1), 'months')
        // Increment day value 20 times
        for (var day = 20; day > 0; day--) {
            // Randomly add 1 or two days to startTime
            startTime = moment(startTime).add(Math.floor((Math.random() * 2) + 1), 'days');
            // Increment h:m:s value four times
            for (var hour = 4; hour >= 0; hour--) {
                // Add random hours between 1 and 3 inclusive
                // -||- minutes -||- 1 and 30
                // -||- seconds -||- 1 and 45

                startTime = moment(startTime).add(Math.floor((Math.random() * 3) + 1), 'hours')
                    .add(Math.floor((Math.random() * 30) + 1), 'minutes')
                    .add(Math.floor((Math.random() * 3) + 1), 'seconds');

                var endTime = moment(startTime).add(pHours, 'hours').add(pMinutes, 'minutes').add(pSeconds, 'seconds');
                var toSaveObject = {
                    starttime: moment(startTime).format("YYYY/MM/DD hh:mm:ss"),
                    endtime: moment(endTime).format("YYYY/MM/DD hh:mm:ss"),
                    duration: pHours + ":" + pMinutes + ":" + pSeconds
                };
                writePomodoroToConfig(toSaveObject);
            }
        }
    }
});
$('#startCountdown').click(()=> {
    switchEnableDisableButton([]);
    // Set variable which timer is running ATM
    timerRunning = "POMODORO";
    // Set timers based on the latest values of config.json
    initializeTimerValues();
    startTime = getCurrentDateTime();
    // Increase pomodoro counter
    pomodoroCounter++;
    switchEnableDisableButton(['#startCountdown','#endCountdown',"#settingsBtn","#showDb","#skipNextCountdown"]);
    console.log("PC: ", pomodoroCounter);
    $('#clockDiv').countdown(pomodoroTime(pHours, pMinutes, pSeconds), (e)=> {

        $("#clockDiv").html(e.strftime('%H:%M:%S', pomodoroCounter));
    });
});

$('#startBreak').click(()=> {
    switchEnableDisableButton(["#startBreak","#endCountdown","#settingsBtn","#showDb",,"#skipNextCountdown"]);
    // Determine which break (long or short) variables to pass to the countdown method
    var consoleIndex;
    var bh = 0;
    var bm = 0;
    var bs = 0;
    if (pomodoroCounter >= 4) {
        bh = lbHours;
        bm = lbMinutes;
        bs = lbSeconds;
        consoleIndex = "Long";
        pomodoroCounter = 0;
    } else {
        bh = sbHours;
        bm = sbMinutes;
        bs = sbSeconds;
        consoleIndex = "Short";
    }
    timerRunning ="BREAK";
    console.log("In ", consoleIndex);
    $('#clockDiv').countdown(pomodoroTime(bh, bm, bs), (e)=> {
        $("#clockDiv").html(e.strftime(' %H:%M:%S'));
    });

});
// Method to extract hours, minutes and seconds from custom (HH:mm:ss) format,
// note: Works only for durations of <= 24h
function sliceHms(timeVar){
    var h = timeVar.slice(0,2);
    var m = timeVar.slice(3,5);
    var s = timeVar.slice(6,8);
    return{
        h : h,
        m : m,
        s : s
    }
}

$('#showDb').click(()=>{


    ipc.send('open-results');
});

$("#endCountdown").click(()=>{
    // IMPLEMENT PAUSING AND RESUMING
    var pauseTime = getCurrentDateTime();
    timeElapsed = pauseTime-startTime;
    console.log(Math.abs(timeElapsed));
    // Time remaining at STOP
    var remainingtime =moment.duration((startTime -pauseTime),"milliseconds").format("HH:mm:ss",{trim:false});

    // Prompt window:
    ipc.send('end-countdown');
    ipc.on('answer',(e,arg)=>{
        // If answer was 'OK'
        if(arg === 0){
            // Pause timer
            $('#clockdiv').countdown('pause');
            // Set flag - indicates that timer ended prematurely
            hasEndedByInput = true;
            // End the countdown - ie countdown from 0 to 0
            $('#clockDiv').countdown(pomodoroTime(0,0,0));
            displayMessageInBottomBar("Pomodoro ended by user.");
            // switchEnableDisableButton(["#settingsBtn","#showDb"]);



        }else{
            // Countdown from the paused time calculated above

            $('#clockDiv').countdown(pomodoroTime(sliceHms(remainingtime).h,
                sliceHms(remainingtime).m,
                sliceHms(remainingtime).s), (e)=> {

                $("#clockDiv").html(e.strftime('%H:%M:%S', pomodoroCounter));

            });
        }
    });


});
// Each time a timer has finished:
$('#clockDiv').on('finish.countdown',()=>{
    console.log("In finish countdown!");
    if(timerRunning === "POMODORO" && hasEndedByInput === false){
        if(config.get('Sound.enabeled')) {
            audio.play();
        }
        // Check if pomodoro has elapsed in it's totality (as per rules we don't count unfinished pomodoros)
        timeElapsed = startTime - getCurrentDateTime();
        console.log(pHours + ":" + pMinutes + ":" + pSeconds);

        var toSaveObject = {
            starttime: startTime.format("YYYY/MM/DD hh:mm:ss"),
            endtime: getCurrentDateTime().format("YYYY/MM/DD hh:mm:ss"),
            duration: pHours + ":" + pMinutes + ":" + pSeconds
        };
        writePomodoroToConfig(toSaveObject);
        displayMessageInBottomBar("Pomodoro finished and written to the database.");
        // Manage enabled/disabled buttons
        $("#clockdiv").html("");
        switchEnableDisableButton(["#startBreak","#endCountdown","#settingsBtn","#showDb","#skipNextCountdown"]);

        }
    //    If timer was ended by pressing a stop button:
    else if(timerRunning === "POMODORO" &&hasEndedByInput === true){

        switchEnableDisableButton(["#startBreak","#endCountdown","#settingsBtn","#showDb","#skipNextCountdown"]);
        hasEndedByInput = false;
    }
    else if(timerRunning === "BREAK"){
        if(hasEndedByInput === false){
            if(config.get('Sound.enabeled')) {
                audio.play();
            }
        }


        switchEnableDisableButton(["#startCountdown","#endCountdown","#settingsBtn","#showDb","#skipNextCountdown"])
    }
});
// Skip button functionality (switches currently active buttons (start countdown / start pause))
$('#skipNextCountdown').click(()=>{
    var buttonIds = ['#startCountdown','#startBreak'];
    switchEnableDisableButton(buttonIds)

});
// Function to switch button from enabled to disabled and vice versa
function switchEnableDisableButton(buttonIds){
    for(var i= 0; i < buttonIds.length; i++)
    {
        if (!($(buttonIds[i]).hasClass('disabled'))) {
            $(buttonIds[i]).prop('disabled', true).addClass('disabled');
        }
        else {
            $(buttonIds[i]).prop('disabled', false).removeClass('disabled');
        }
    }
}
// Simple method for displaying messages in footer 'command line'
function displayMessageInBottomBar(message) {
    $("#footerText").typed({
        strings: [message],
        typeSpeed: 0
    });

}
/*SETTINGS PAGE:*/
$('#settingsBtn').click(()=> {
    // Send message open-settings to main.js
    ipc.send('open-settings');
});
