// /**
//  * Created by Saso on 10/16/2016.
//  */
 const electron = require('electron');
 const {ipcRenderer: ipc,remote} = electron;
// Access to configuration file
const Config = require('electron-config');
const config = new Config();

$(document).ready(()=>{
    document.getElementsByTagName("html")[0].style.visibility = "visible";
});


$('.closeSettings').click(()=>{
     ipc.send('close-settings')
 });
// Save new values from settings menu to config.json
$('#saveSettings').click(()=>{
    // Save new pomodoro timer values:
    config.set('PomodoroTimer.hours', $('#pomodorodatetimepicker').find('span.timepicker-hour').text());
    config.set('PomodoroTimer.minutes',$('#pomodorodatetimepicker').find('span.timepicker-minute').text());
    config.set('PomodoroTimer.seconds',$('#pomodorodatetimepicker').find('span.timepicker-second').text());
    // Save new short break timer values:
    config.set('BreakTimerShort.hours', $('#shortbreakdatetimepicker').find('span.timepicker-hour').text());
    config.set('BreakTimerShort.minutes',$('#shortbreakdatetimepicker').find('span.timepicker-minute').text());
    config.set('BreakTimerShort.seconds',$('#shortbreakdatetimepicker').find('span.timepicker-second').text());
    // Save new long break timer values:
    config.set('BreakTimerLong.hours', $('#longbreakdatetimepicker').find('span.timepicker-hour').text());
    config.set('BreakTimerLong.minutes',$('#longbreakdatetimepicker').find('span.timepicker-minute').text());
    config.set('BreakTimerLong.seconds',$('#longbreakdatetimepicker').find('span.timepicker-second').text());
    // Save new cylce counter values:
    config.set('Sequence.pomodorosBeforeLong',$('#cylcesbeforelongpicker').find('span.timepicker-second').text());
    config.set('Debug.checked',$('#debug').is(':checked'));

    ipc.send('saved-settings');
    ipc.send('close-settings');
 });

$('#revertSettings').click(()=>{
    ipc.send('revert-to-default');
    ipc.send('close-settings');
});

// Datetimepicker widgets settings:
// Since we're not using current time, set time to values in config file w/ moment.js

function readFromSettings(){

    $('#pomodorodatetimepicker').datetimepicker({
        useCurrent: false,
        defaultDate: moment(
            {
                hour: config.get('PomodoroTimer.hours'),
                minute: config.get('PomodoroTimer.minutes'),
                seconds:config.get('PomodoroTimer.seconds')
            }),
        inline: true,
        format: 'HH mm ss'


    });
    $('#shortbreakdatetimepicker').datetimepicker({
        inline: true,
        defaultDate: moment(
            {
                hour: config.get('BreakTimerShort.hours'),
                minute: config.get('BreakTimerShort.minutes'),
                seconds: config.get('BreakTimerShort.seconds')
            }),
        format: 'HH mm ss',
        useCurrent: false
    });
    $('#longbreakdatetimepicker').datetimepicker({
        inline: true,
        defaultDate: moment(
            {
                hour: config.get('BreakTimerLong.hours'),
                minute: config.get('BreakTimerLong.minutes'),
                seconds: config.get('BreakTimerLong.seconds')
            }),
        format: 'HH mm ss',
        useCurrent: false
    });
    // A little cheat, for consistency of design I'll use seconds picker as cycle picker as well
    $('#cylcesbeforelongpicker').datetimepicker({
        inline: true,
        defaultDate: moment({seconds: config.get('Sequence.pomodorosBeforeLong') }),
        format: 'ss',
        useCurrent: false
    });
    // Read from settings if Debug mode is enabled
    $('#debug').attr('checked',config.get('Debug.checked'));


    /*
    * if($("#isAgeSelected").is(':checked'))
     $("#txtAge").show();  // checked
     else
     $("#txtAge").hide();  // unchecked*/
}
// Customize inline widget input:
$(document).ready(()=>{
    readFromSettings();
    $('div.form-group').find('span.timepicker-minute').before("Minutes");

    $('.timewidget').find('span.timepicker-second').before("Seconds");


    $('#cyclcehack').find('span.timepicker-second').before("Cycles");


    $('div.form-group').find('span.timepicker-hour').before("Hours");

});
