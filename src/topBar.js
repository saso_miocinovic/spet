/**
 * Created by Saso on 10/28/2016.
 */
(
    ()=> {
        const electron = require('electron');
        const {ipcRenderer: ipc,remote}= electron;

        // Listen for click, get current window and add basic functionality
        function init() {
            document.getElementById('min-btn').addEventListener("click",(evt)=>{
                const win = remote.getCurrentWindow();
                win.minimize();
            });
            // document.getElementById('max-btn').addEventListener("click",(evt)=>{
            //     const win = remote.getCurrentWindow();
            //     if (!win.isMaximized()) {
            //         win.maximize();
            //     }else{
            //         win.unmaximize();
            //     }
            // });
            document.getElementById('close-btn').addEventListener("click",(evt)=>{
                const win = remote.getCurrentWindow();
                win.close();
            });
            $('#info-btn').on('click',()=>{
                ipc.send('open-info');
            });
            
            
        };

        document.onreadystatechange = ()=>{
            if(document.readyState == "complete"){
                init();
            }
        };

    }
)();