/**
 * Created by Saso on 10/5/2016.
 */

/****************************************-*/
/*ELECTRON*/
const electron = require('electron');
const {ipcMain:ipc, BrowserWindow, app, Menu, dialog,remote}= electron;
/****************************************-*/

/*NODE*/
/****************************************-*/
var fs = require('fs');

/****************************************-*/
// Path to db file:
var dbPath = app.getPath('userData');
/****************************************-*/
// Persistent settings configurations & NeDB
const Config = require('electron-config');
const config = new Config();


/****************************************-*/

/****************************************-*/
// Only for gulp tasks, needs to be removed to build a solution
// const client = require('electron-connect').client;
/****************************************-*/

/****************************************-*/
// Windows:
var win;
var infoWin = null;
var settingsWin = null;
var resultsWin = null;
/****************************************-*/

/****************************************-*/
// Control variables
let willQuit = false;


/****************************************-*/


/****************************************-*/

/****************************************-*/



app.on('window-all-closed', (e)=> {
    app.quit();

});

app.on('ready', ()=> {

    var width= 606;
    var height= 292;

    // Create default configuration file if it doesn't exist already:
    var filepath = config.path;
    //(config.path);

    // Read config file synchronously if it's length is <= 3 it's an empty JSON object (ie parentheses only)
    var contentLength = fs.readFileSync(filepath).toString().length;

    // If tere's empty JSON in file  or if file does not exist create a default setting content
    //console.log(contentLength);
    //console.log("PATH: "+config.path);
    if (contentLength <= 2 || contentLength === undefined) {
        StartupValuesConfiguration();
    } else {
        //console.log("Config is fine.");
    }

    /*Menu config:*/

    win = new BrowserWindow({width, height,frame:false,show:false,resizable: false});
    win.once('ready-to-show',()=>{
        win.show();
    });
    // win.setMenu(null);
    //Set the basic variable values to the JSON file
    // console.log("app.getPath",);
    win.loadURL('file://' + __dirname + '/mainDisplay.html');
    // win.webContents.openDevTools();

    // For live reloading
    // client.create(win);
    /*HANDLE win CLOSE EVENT:*/
    //Close event when clicking 'X' returns an index of an array (ie 0 or 1)
    win.on('close', (e)=> {

        var answer = dialog.showMessageBox({
            message: "If you quit all running tomatoes could be lost!",
            buttons: ["OK", "Cancel"]
        });
        // console.log(answer);
        if (answer === 0) {
            // console.log("WQ: ",willQuit);
            willQuit = true;

        }
        if (answer === 1) {
            // console.log("WQ: ",willQuit);
            willQuit = false;
            e.preventDefault();

        }
        // app.quit();
    });

    // before-quit is connected to close event (WHEN OS SHUTS DOWN):
    app.on('before-quit', (e)=> {

        if (willQuit === false) {
            // console.log("BQ WQ=FALSE");
            e.preventDefault();
        }
        else if (willQuit === true) {
            // console.log("BQ WQ=TRUE");
        }
    });
    app.on('activate-with-no-open-windows', ()=> {
        win.show();
    });

});

// Another way to handle events after all windows are closed:
app.on('will-quit', (e)=> {


    if (willQuit === true) {
        // console.log("will-quit");
        win = null;
    } else {
        e.preventDefault();
    }


});
function createWindow(name){
    // Reference the correct window
    if(name === 'results')
        window = resultsWin;
    else if (name === 'settings')
        window = settingsWin;
    else if(name === 'info')
        window = infoWin;
    // Don't open more than one instance!
    if(window){
        return;
    }

    //console.log("In create window");
    window = new BrowserWindow({
        frame: false,
        resizable: false,
        parent: win,
        height: 685,

        width: 600

    });
    console.log("URL to load: ",'file://' + __dirname + '/'+name+'.html');
    window.loadURL('file://' + __dirname + '/'+name+'.html');
     // window.webContents.openDevTools();

        window.on('closed',()=>{
            window = null;
        });



}
ipc.on('open-results',()=>{

    createWindow('results');
});

// Listen to message to open settings windows from mainDisplay.js
ipc.on('open-settings', ()=> {


    createWindow('settings');
});
ipc.on('close-settings',()=>{
    BrowserWindow.getFocusedWindow().close();

});
// Show dialog that settings have been saved
ipc.on('saved-settings',()=>{

    dialog.showMessageBox({
        message: "Your settings have been saved.",
        buttons: ["OK"]
    });
    // Close settings window
    BrowserWindow.getFocusedWindow().close();
    //Reload main window to apply new settings
    win.reload();
});
ipc.on('revert-to-default',()=>{
    //console.log("RTD");
    StartupValuesConfiguration();
    dialog.showMessageBox({
        message: "Your settings have been reverted to default values.",
        buttons: ["OK"]
    });
    
});
ipc.on('end-countdown',(e,arg)=>{
    var answer = dialog.showMessageBox({
        message: "Do you really want to end a running pomodoro?",
        buttons: ["End Timer","Resume"]
    });
    e.sender.send('answer',answer);
});
ipc.on('open-info',()=>{
    createWindow('info');
});
/***** DEFAULT CONFIGURATION: *****/
/*Will save config.json in %APPDATA%\Roaming\pomodoro\ on Windows machines*/
function StartupValuesConfiguration() {
    config.set({
            "PomodoroTimer": {
                "hours": 0,
                "minutes": 25,
                "seconds": 0
            },

            "BreakTimerShort": {
                "hours": 0,
                "minutes": 5,
                "seconds": 0
            },


            "BreakTimerLong": {
                "hours": 0,
                "minutes": 25,
                "seconds": 0
            },
            "DbFilePath": dbPath+"\\pomodorodbfile.json",

            "Sequence": {
                "pomodorosBeforeLong": 4
            },
            "Sound":{
                "enabeled" : true
            },
            "Debug":{
                "checked" : false
            }


        }

    );



}
