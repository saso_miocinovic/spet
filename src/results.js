/**
 * Created by Saso on 10/29/2016.
 */
const electron = require('electron');
const Config = require('electron-config');
const config = new Config();

const Datastore = require('nedb');
var pomodoros= new Datastore({filename: config.get('DbFilePath'), autoload: true, corruptAlertThreshold: 1});

$(document).ready(()=>{
    pomodoros.find({},(err,docs)=>{
        // Insert JSON (NeDB) data into datatables
        // _id is meaningless here and is just added for testing purposes.
        $('#example').dataTable({
            "aaData" : docs,
            "aoColumns" : [
                
                {"mDataProp": "starttime"},
                {"mDataProp": "endtime"},
                {"mDataProp": "duration"}
            ]


        });

    });
});
